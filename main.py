# This is a sample Python script.
from sys import stdin


def newq():
    import boto3

    # Press ⌃R to execute it or replace it with your code.
    # Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
    sqs = boto3.resource('sqs')

    queue = sqs.create_queue(QueueName='test', Attributes={'DelaySeconds': '5'})
    print(queue.url)
    print(queue.attributes.get('DelaySeconds'))


def storage():
    import boto3

    # Press ⌃R to execute it or replace it with your code.
    # Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
    faws = boto3.resource('s3')

    for bucket in faws.buckets.all():
        print(bucket.name)


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.


def lqueue():
    import boto3
    # Get the service resource
    sqs = boto3.resource('sqs')

    # Get the queue. This returns an SQS.Queue instance
    queue = sqs.get_queue_by_name(QueueName='test')

    # You can now access identifiers and attributes
    print(queue.url)
    print(queue.attributes.get('DelaySeconds'))

    for queue in sqs.queues.all():
        print(queue.url)


def smessage(data):
    import boto3

    print (data)
    sqs = boto3.resource('sqs')
    queue = sqs.get_queue_by_name(QueueName='test')
    response = queue.send_messages(Entries=[
        {
            'Id': '1',
            'MessageBody': 'world'
        },
        {
            'Id': '2',
            'MessageBody': 'boto3',
            'MessageAttributes': {
                'Author': {
                    'StringValue': 'Daniel',
                    'DataType': 'String'
                }
            }
        }
    ])
    print(response.get('Failed'))
    print(response.get('MessageId'))
    print(response.get('MD5OfMessageBody'))


def proceses():
    import boto3

    sqs = boto3.resource('sqs')
    queue = sqs.get_queue_by_name(QueueName='test')
    for message in queue.receive_messages(MessageAttributeNames=['Author']):
        # Get the custom author message attribute if it was set
        author_text = ''
        if message.message_attributes is not None:
            author_name = message.message_attributes.get('Author').get('StringValue')
            print(author_name)
            if author_name:
                author_text = ' ({0})'.format(author_name)
                print(author_text)

    # Print out the body and author (if set)
    print('Hello, {0}!{1}'.format(message.body, author_text))

    # Let the queue know that the message is processed
    message.delete()


def gname():
    nombre = input("Mi nombre es:")
    return (nombre)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    quienes = gname()
    lqueue()
    smessage(quienes)
    proceses()
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
